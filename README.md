AXCESS Box Mobile Storage is a fast growing mobile storage company involved in the sale and rental of new and used steel storage containers for the retail market. With the recent introduction of its ‘rent-to-own’ program, AXCESS BOX now provide inhouse financing for new and used container purchases.

Address: 34466 4th Ave, Abbotsford, BC V2S 8B6, Canada

Phone: 778-771-4728

Website: https://www.axcessbox.com
